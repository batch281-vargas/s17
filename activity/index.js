/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	let personDetails = function () {
		let fullName = prompt('Enter your name:')
		console.log('Hello, ' + fullName);
		let age = prompt('Enter age:')
		console.log('You are ' + age + ' years old.');
		let location = prompt('Enter your location:')
		console.log('You live in ' + location + ' City');
	}

	personDetails();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

console.log('================================================')

	//second function here:
	let musicArtists = function () {
		
		let artist1 = 'Slapshock';
		let artist2 = 'Greyhounds';
		let artist3 = 'Eraserheads';
		let artist4 = 'Blink 182';
		let artist5 = 'Wolfgang';

		console.log('1. '+ artist1)
		console.log('2. '+ artist2)
		console.log('3. '+ artist3)
		console.log('4. '+ artist4)
		console.log('5. '+ artist5)
	}

	musicArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

console.log('================================================')
	
	//third function here:

let moviesFunction = function () {

		let movieTitle1 = '1. The GreenMile';
		let movieRating1 = 'Rotten tomatoes Rating: 99%';
		let movieTitle2 =  '2. Aqua Man';
		let movieRating2 = 'Rotten tomatoes Rating: 92%';
		let movieTitle3 = '3. Avengers';
		let movieRating3 = 'Rotten tomatoes Rating: 93%';
		let movieTitle4 = '4. Spiderman';
		let movieRating4 = 'Rotten tomatoes Rating: 94%';
		let movieTitle5 = '5. Superman';
		let movieRating5 = 'Rotten tomatoes Rating: 96%';

		console.log(movieTitle1);
		console.log(movieRating1);
		console.log(movieTitle2);
		console.log(movieRating2);
		console.log(movieTitle3);
		console.log(movieRating3);
		console.log(movieTitle4);
		console.log(movieRating4);
		console.log(movieTitle5);
		console.log(movieRating5);
	}

	moviesFunction();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printUsers();

// console.log(friend1);
// console.log(friend2);